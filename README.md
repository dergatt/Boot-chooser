This script provides a easy method for restoring the state from last boot.

This sricpt works with two Brtfs subvolumes both of them are systemdboot entries.
5 minutes after boot the non used subvolume is replaced by the state of the choosen one on boot.
The default boot entry is changeg depending on which boot entry is booted.


Requires:
- / is a btrfs subvolume 
- The entire drive is mounted somewhere under /
- systemdboot as bootloader
- 2 Btrfs subvolumes to boot from
- 2 boot entries named root1 and root2 s referring to both subvolumes
